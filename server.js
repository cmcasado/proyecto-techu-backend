//variable para levantar el servidor express
var express = require('express')
//variable para parsear el req.body
var bodyParser = require('body-parser')
//variable para utilizar request-json
var requestJson = require ('request-json')
//variable para levantar la aplicación
var app = express()
//variable para indicar el puerto en el que va a escuchar la aplicación
var port = process.env.PORT || 3000
//se indica a la aplicación el puerto en que escuchar
app.listen(port, function() {
  console.log("API escuchando en el puerto "+port)
})
//se indica a la aplicación que use el bodyParser para parsear json
app.use(bodyParser.json())

//se carga la configuración para conectar a la BBDD
var config = require('./config/config.json')
var urlMlabRaiz = config.urlMlabRaiz
var apiKey = config.apiKey
var clienteMlab = null

//Endpoint de entrada al API
app.get('/apitechu', function(req, res){
    res.json({"mensaje": "Bienvenido a la API REST 'ApiTechu' de Toño y Carlos"})
})

//recurso de Cliente
require('./app/cliente/cliente.js')(app, requestJson, urlMlabRaiz, apiKey)
//recurso de Cuenta
require('./app/cuenta/cuenta.js')(app, requestJson, urlMlabRaiz, apiKey)
