
module.exports = function(app, requestJson, urlMlabRaiz, apiKey) {
  //variable para cifrar las contraseñas
  var bcrypt = require('bcrypt');

  /**
  * Devolver lista de usuarios
  */
  app.get('/apitechu/v1/usuarios', function(req, res){
   //instancio la variable con la petición que voy a realizar a Mlab
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + apiKey)
   //hago la petición a la API de Mlab
   clienteMlab.get('', function(err, resM, body){
     if(!err){
       //si todo ha ido bien, devuelvo la lista de usuarios que está en body
       res.status(200).send(body)
     }
     else{
       res.status(404).send("Error: "+ err)
     }
   })
  })

  /**
  * Devolver todos los datos de un usuario que se pase por ID
  */
  app.get('/apitechu/v1/usuarios/:id', function(req, res){
    //se recoge el id a consultar por parámetro
    var id = req.params.id
    //construcción de query para obtener solo nombre y apellido
    //se establece l=1 para que solo recoger un registros, ya que por diseño he garantizado que el id es único
    var query = 'q={"id":'+id+'}&f={"id":1,"nombre":1, "apellido":1, "ciudad":1, "email":1,"fec_naci":1,"genero":1,"telefono":1, "_id":0}&l=1&'
    //construcción de query
    clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' +query+ apiKey)
    //petición a la API de Mlab
    clienteMlab.get('', function(err, resM, body){
      if(!err){
        //si tiene elementos
        if (body.length > 0){
          //el usuario recibe el body
          res.send(body[0])
        }
        else{
          res.status(404).send("Usuario no encontrado")
        }
      }
      else{
        res.send()
      }
    })
  })

  /**
  * Realizar Login
  */
  app.post('/apitechu/v1/login', function(req, res){

   //usuario y password recibido por cabecera
   var email = req.headers.email
   var password = req.headers.password

   //construcción de query
   var query = 'q={"email":'+'"'+email+'"'+', "password":'+'"'+password+'"'+'}&f={"id":1, "nombre":1, "apellido":1, "_id":0}&l=1&'
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)

   //petición a la API Mlab
   clienteMlab.get('', function(errM, resM, body){
     if(!errM){
       //si hay algún registro, implica que Login OK
       if (body.length == 1){
         //actualiza el atributo logged en la BBDD para el cliente que ha logado
         //construcción de query
         clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+body[0].id+'}&'+apiKey)
         //atributo a modificar
         var cambio = '{"$set":{"logged":true}}'

         //petición PUT para actualizar la BBDD con el cambio
         clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
           if(!errP){
             //devuelve resultado al usuario
             res.send({"Login":"OK", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
           }
           else{
             res.send(errP)
           }
         })
       }
       else{
         res.status(404).send({"Error":"Usuario no encontrado o Password incorrecto"})
       }
     }
     else{
       res.send()
     }
   })
  })

  /**
  * Realizar Login con cifrado
  */
  app.post('/apitechu/v2/login', function(req, res){

   //usuario y password recibido por cabecera
   var email = req.headers.email
   var password = req.headers.password

   //construcción de query
   var query = 'q={"email":'+'"'+email+'"'+'}&f={"id":1, "nombre":1, "apellido":1, "_id":0, "password":1}&l=1&'
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)
   //petición a la API Mlab
   clienteMlab.get('', function(errM, resM, body){
     if(!errM && body.length >0){
       //console.log("este es el ResM")
       //console.log(resM)
       console.log("este es el errM")
       console.log(errM)
       console.log("este es el body")
       console.log(body)
       //cifrado del password que llega al API
        //este 'hash' es lo que habría que almacenar en la BBDD
       var hash = bcrypt.hashSync(password, 10)
       //comparación con lo almacenado en BBDD
       //y si coincide...
       if(bcrypt.compareSync(password, body[0].password)) {
         //actualiza el atributo logged en la BBDD para el cliente que ha logado
         //construcción de query
         clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+body[0].id+'}&'+apiKey)
         //atributo a modificar
         var cambio = '{"$set":{"logged":true}}'

         //petición PUT para actualizar la BBDD con el cambio
         clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
           if(!errP){
             //devuelve resultado al usuario
             res.send({"Login":"OK", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
             //AQUI Añadimos también la parte de insertar el nuevo movimiento.
           }
           else{
             res.send(errP)
           }
         })
       }
       else{
         res.status(404).send({"Error": "Usuario o Password erróneos"})
       }
     }
     else{
       res.status(404).send({"Error": "Usuario o Password erróneos"})
     }
   })
  })

  /**
  * Realizar Logout
  */
  app.post('/apitechu/v1/logout', function(req, res){

   //id recibido por cabecera
   var id = req.headers.id

   //construcción de query
   var query = 'q={"id":'+id+', "logged":true}&f={"id":1, "nombre":1, "apellido":1, "_id":0}&l=1&'
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + apiKey)

   //petición a la API Mlab
   clienteMlab.get('', function(errM, resM, body){
     if(!errM){
       //si hay algún registro, implica que estaba logado
       if (body.length == 1){
         //actualiza el atributo logged en la BBDD para el cliente que ha realizado logout
         //construcción de query
         clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+body[0].id+'}&'+apiKey)
         //atributo a modificar
         var cambio = '{"$set":{"logged":false}}'

         //petición PUT para actualizar la BBDD con el cambio
         clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP){
           if(!errP){
             //devuelve resultado al usuario
             res.send({"Logout":"OK", "id":body[0].id})
           }
           else{
             res.end(errP)
           }
         })
       }
       else{
         res.status(200).send({"Error": "Usuario no logado previamente"})
       }
     }
     else{
       res.send()
     }
   })
  })

  /**
   * Validar los campos del formulario de registro
  */
        function _validarFormulario(nombre,apellido,ciudad,email,password,fec_naci,genero,telefono){
          var msgError = ""
          // no se aceptan valores nulos en ningún atributo
          if(nombre == null || apellido == null || ciudad == null || email == null || password == null || fec_naci == null || genero == null || telefono == null){
              msgError = "Ningún campo del formulario puede estar vacío"
              return msgError
          }
          else{
            if(email == ""){
              msgError = "Ningún campo del formulario puede estar vacío"
              return msgError
            }
          }

    // validar formato del email
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!filter.test(email)){
        msgError = "El campo 'Email' debe contener una dirección de correo válida"
        return msgError
    }
    return msgError
  }
  /**
   * Validar los campos del formulario de registro
  */
        function _validarFormularioModif(nombre,apellido,ciudad,email,fec_naci,genero,telefono){
          var msgError = ""
          // no se aceptan valores nulos en ningún atributo
          if(nombre == null || apellido == null || ciudad == null || email == null || fec_naci == null || genero == null || telefono == null){
              msgError = "Ningún campo del formulario puede estar vacío"
              return msgError
          }
          else{
            if(email == ""){
              msgError = "Ningún campo del formulario puede estar vacío"
              return msgError
            }
          }

    // validar formato del email
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!filter.test(email)){
        msgError = "El campo 'Email' debe contener una dirección de correo válida"
        return msgError
    }
    return msgError
  }
/**
 *  Endpoint de alta de Usuarios, recibe la información por Headers
 */

      app.post('/apitechu/v1/usuarios/alta', function(req,res)
        {
          var nombre = req.headers.nombre
          var apellido = req.headers.apellido
          var email = req.headers.email
          var ciudad = req.headers.ciudad
          var password = req.headers.password
          var fec_naci = req.headers.fec_naci
          var genero = req.headers.genero
          var telefono = req.headers.telefono
          var hash = bcrypt.hashSync(password, 10)

     //Se llama a la función para validar el contenido de los campos de registro de Usuario
          var mensajeError = _validarFormulario(nombre,apellido,ciudad,email,password,fec_naci,genero,telefono)
               if (mensajeError != "") {
             res.status(404).send(mensajeError)
          }
          else {
          //Query para validar que el email no existe
          var query_v = 'q={"email":'+'"'+email+'"'+'}&f={"id":1, "nombre":1, "apellido":1,"email":1, "_id":0, "password":1}&l=1&'
          //Query para obtener el ID más alto de la BBDD e insrtar con ese +1
          var query_s = 'f={"id":1, "_id":0}&l=1&s={"id":-1}&'
                clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query_s + apiKey)
                //petición a la API Mlab para obtener el ID MAs alta de la colección
                clienteMlab.get('', function(errI, resI, bodyI){
           var nuevoid = (parseInt(bodyI[0].id) + 1)
           //Comprobamos que el Email no exista ya en nuestra BBDD. Y en caso de existir no la dejamos dar el alta
           clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query_v + apiKey)
           clienteMlab.get('', function(errM, resM, bodyM){
           // El tamaño del Body del Get es cero si no existe el Email.
           if (bodyM.length == 0)
     		{
     		//Como no existe ya si damos el alta
     		clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
        //esta parte es para el alta en la coleccion de cliente
var query_usuario = '{"id":'+nuevoid+',"nombre":'+'"'+nombre+'"'+',"apellido":'+'"'+apellido+'"'+',"ciudad":'+'"'+ciudad+'"'+',"email":'+'"'+email+'"'+',"fec_naci":'+'"'+fec_naci+'"'+',"genero":'+'"'+genero+'"'+',"telefono":'+'"'+telefono+'"'+',"password":'+'"'+hash+'"'+'}'
        //esta parte es para el alta en la coleccion de cuentas
        var query_cuentas = '{"idcliente":'+nuevoid+',"saldo":'+0+'}'
        clienteMlab.post('',JSON.parse(query_usuario),function(errP,resP,bodyP){
     			if (!errP){
                  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + apiKey)
                  clienteMlab.post('',JSON.parse(query_cuentas),function(errC,resC,bodyC){})
                   res.status(200).send("El usuario " + email +" ha sido dado de alta")
     				        }
     				else { res.status(404).send("Error realizando el alta")}
     					})

     		}

            else{ res.status(404).send("El email " + bodyM[0].email +" ya existe en la BBDD")}
     	 })
     	})
    }
      })

/**
 * Endpoint de Modificación de Usuarios, recibe la información por Headers
 */
 app.post('/apitechu/v1/usuarios/modificacion', function(req,res)
{
  var nombre = req.headers.nombre
  var apellido = req.headers.apellido
  var email = req.headers.email
  var ciudad = req.headers.ciudad
  var fec_naci = req.headers.fec_naci
  var genero = req.headers.genero
  var telefono = req.headers.telefono
//Se llama a la función para validar el contenido de los campos de registro de Usuario
  var mensajeError = _validarFormularioModif(nombre,apellido,ciudad,email,fec_naci,genero,telefono)
       if (mensajeError != "") {
     res.status(404).send(mensajeError)
  }
  else {
  //Query para validar que el email existe en BBDD y por lo tanto le dejamos al usuario actualziar sus datos
  var query_v = 'q={"email":'+'"'+email+'"'+'}&f={"id":1,"nombre":1,"email":1, "_id":0, "password":1}&l=1&'
   clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query_v + apiKey)
   //petición a la API Mlab
   clienteMlab.get('', function(errM, resM, bodyM){
   // El tamaño del Body del Get es cero si no existe el Email.
   if (bodyM.length != 0){
   //Como existe ya si actualizamos los datos del Cliente.
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?q={"id":'+bodyM[0].id+'}&'+apiKey)
  //Query para actualizar los datos del Usuario.
var query = '{"$set":{"nombre":'+'"'+nombre+'"'+',"apellido":'+'"'+apellido+'"'+',"ciudad":'+'"'+ciudad+'"'+',"fec_naci":'+'"'+fec_naci+'"'+',"genero":'+'"'+genero+'"'+',"telefono":'+'"'+telefono+'"'+'}}'
  clienteMlab.put('',JSON.parse(query),function(errP,resP,bodyP){
    if (!errP){
               res.status(200).send("Los datos del usuario " + bodyM[0].email +" han sido actualizados")
    }
    else { res.status(404).send("Error realizando la actualizacion")
         }
  })
}

  //else{res.send("El email ya existe en la BBDD")
  else{ res.status(404).send("Error al actualizar, el email " + email +" no existe en la BBDD")}
  })
}
})
}
