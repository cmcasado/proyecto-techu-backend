module.exports = function(app, requestJson, urlMlabRaiz, apiKey) {
    /**
     * Devuelve las cuentas de un cliente dado su ID
     */
    app.get('/apitechu/v1/cuentas/cliente', function(req, res){
        //idCliente recibido por cabecera
        var idcliente = req.headers.idcliente

        //construcción de query
        var query = 'q={"idcliente":'+idcliente+'}'
        var filter = 'f={"iban":1, "saldo":1, "_id":0}'
        var ordenamiento = 's={"iban": 1}'
        clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + ordenamiento + "&" + filter + "&" + apiKey)
        //petición a la API Mlab
        clienteMlab.get('', function(errC, resC, body){
          if(!errC){
            //si ha encontrado IBANes asociados a un idcliente
            if (body.length > 0){
                  //al usuario se devuelve por pantalla el listado de ibanes
                  res.status(200).send(body)
            }
            else{
              res.status(404).send("Cuentas no encontradas asociadas al usuario:"+idcliente)
            }
          }
          else{
              res.status(404).send(errC)
          }
        })
    })

    /**
     * Devuelve los movimientos de una cuenta dado un iban concreto
     */
    app.get('/apitechu/v1/cuentas/movimientos', function(req, res){
        //IBAN recibido por cabecera
        var iban = req.headers.iban

        //construcción query
        var query = 'q={"iban":'+"'"+iban+"'"+'}'
        var filter = 'f={"movimientos":1, "_id":0}'
        clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query + "&" + apiKey)
console.log(clienteMlab)
        //petición a la API Mlab
        clienteMlab.get('', function(errM, resM, body){
          if(!errM){
            //si ha encontrado movimientos asociados a un IBAN
            if (body.length > 0){
                  //al usuario le devuelvo por pantalla el listado de ibanes
                  res.status(200).send(body)
            }
            else{
              res.status(404).send("Movimientos no encontradas asociadas al IBAN:"+iban)
            }
          }
          else{
              res.status(404).send(errM)
          }
        })
    })
    //Endpoint de alta de movimientos, recibirá la información por Headers
       app.post('/apitechu/v1/cuentas/movimientos/alta', function(req,res)
       {
    //recibimos el Iban de la cuenta el tipo de movimiento y el saldo del movimiento.
    //Validaremos que el movimiento si es trasnferencia no exceda el importe del SAldo de la cuenta.
    // Si pasa las validaciones de saldo incrementeramos o disminuiremos el saldo según corresponda.
      var iban = req.headers.iban
    //si recibo un cero es transferencia. Si recibo un uno es ingreso
      var tipomov = req.headers.tipomov
      var saldomov = req.headers.saldomov
      var moneda = req.headers.moneda
      //creo la fecha de hoy para que el nuevo movimiento lleve la fecha.
      var hoy = new Date();
      var dd = hoy.getDate();
      var mm = hoy.getMonth()+1;
      var yyyy = hoy.getFullYear();
        if(dd<10) {
            dd='0'+dd
                  }
        if(mm<10) {
            mm='0'+mm
                  }
      hoy = yyyy+'/'+mm+'/'+dd;
      var query_v = 'q={"iban":'+'"'+iban+'"'+'}&f={"saldo":1,"iban":1,"movimientos":1, "_id":0}&l=1&'
      clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?' + query_v + apiKey)
      //petición a la API Mlab
      clienteMlab.get('', function(errM, resM, bodyM){
        if (bodyM.length!=0){
          var movimientos = bodyM[0].movimientos
          var idmov = (bodyM[0].movimientos.length + 1)
          if (tipomov == 0){
            var nsaldomov = (saldomov*-1)
          var nuevomov = '{"id":'+idmov+',"fecha":'+'"'+hoy+'"'+',"importe":'+nsaldomov+',"moneda":'+'"'+moneda+'"'+'}'
           //convertimos el nuevo movimiento en un JSON para que no lo inserte como una cadena de caracteres
          nuevomov = JSON.parse(nuevomov);
                          }
          else {
            console.log("entro en el else")
            var nsaldomov = (saldomov)
            console.log(nsaldomov)
            var nuevomov = '{"id":'+idmov+',"fecha":'+'"'+hoy+'"'+',"importe":'+nsaldomov+',"moneda":'+'"'+moneda+'"'+'}'
            console.log("pintamos el nuevo movi miento")
            //convertimos el nuevo movimiento en un JSON para que no lo inserte como una cadena de caracteres
            nuevomov = JSON.parse(nuevomov);
            console.log(nuevomov)
                }
          //ahora inserto el nuevo elemnto en los movimientos que tenía
          movimientos.push(nuevomov)
            if (tipomov == 0){
              //como es una transferencia reducimos el saldo e incluimos el movimiento en la cuenta
              var stringify = JSON.stringify(bodyM[0].saldo-saldomov)
              content = JSON.parse(stringify);
              var setsaldo = '{"$set":{"saldo" :'+ content +', "movimientos":'+JSON.stringify(movimientos)+'}}'
           clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?q={"iban":'+ '"' +bodyM[0].iban+'"}&'+ apiKey)
			}
			else{
              //como es un ingreso aumentamos el saldo e incluimos el movimiento en la cuenta
              console.log("body saldo")
              console.log(bodyM[0].saldo)
              console.log("saldo del movimiento")
              console.log(saldomov)
              console.log("suma de los dos")
              saldo=(bodyM[0].saldo-saldomov)
              console.log(saldo)
              var stringify = JSON.stringify(bodyM[0].saldo-(saldomov)*-1)
              content = JSON.parse(stringify);
              var setsaldo = '{"$set":{"saldo" :'+ content +', "movimientos":'+JSON.stringify(movimientos)+'}}'
           clienteMlab = requestJson.createClient(urlMlabRaiz + '/cuentas?q={"iban":'+ '"' +bodyM[0].iban+'"}&'+ apiKey)
			}
          clienteMlab.put('', JSON.parse(setsaldo), function(errP,resP,bodyP){
             if (!errP){
                //res.status(200).send("Operación Realizada")
                res.send({"Operacion":"OK","saldo anterior":bodyM[0].saldo,"saldonuevo":content})
                            }
             else {
               res.send({"Operacion":"Error realizando la operación","Iban":iban})
                  }
           })
         }
         else {
           res.send({"Operacion":"Error realizando la operación","Iban":iban})
              }
           })
       })
}
