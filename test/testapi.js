//se importan los paquetes necesarios
var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')

//instancio mi aplicación, ya que los test unitarios deben ser capaces de levantar la aplicación y cerrarla al finalizar las pruebas
var server = require('../server')

//activa pruebas de tipo "should" (debería). Hay de otros tipos.
var should = chai.should()

//configura chai con módulo HTTP
chai.use(chaiHttp)

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.es') //preparo la request
        .get('/') //le hago el get a la URL
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})


describe('Tests de API usuarios', () => {
  it('Raíz OK', (done) => {
    chai.request('http://localhost:3000') //preparo la request
        .get('/apitechu') //le hago el get a la URL
        .end((err, res) => {
          res.should.have.status(200)
          //hay que usar el should be porque va a acceder a una propiedad del json que devuelve el api (el 'mensaje')
          res.body.mensaje.should.be.eql("Bienvenido a la API REST 'ApiTechu' de Toño y Carlos")
          done()
        })
  })

  it('Lista de usuarios', (done) => {
    chai.request('http://localhost:3000') //preparo la request
        .get('/apitechu/v1/usuarios') //le hago el get a la URL
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('email')
            res.body[i].should.have.property('password')
          }
          done()
        })
  })
})

/*
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {

  })
})
*/
